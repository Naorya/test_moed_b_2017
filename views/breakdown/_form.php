<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Level;
use app\models\Status;
/* @var $this yii\web\View */
/* @var $model app\models\Breakdown */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="breakdown-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

   <?= $form->field($model, 'level')->dropDownList(Level::getLevels()) ?>

      <?=($model->isNewRecord)? $form->field($model,'status')->hiddenInput(['value' => '1'])->label(false) : 
    $form->field($model, 'status')->dropDownList(Status::getStatuses()); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
