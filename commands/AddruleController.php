<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

class AddruleController extends Controller
		{
			public function actionOwnuser()
					{	
						$auth = Yii::$app->authManager;	
						$rule = new \app\rbac\OwnUserRule;
						$auth->add($rule);
					}
					
			public function actionOwnview()
					{	
						$auth = Yii::$app->authManager;	
						$rule = new \app\rbac\OwnViewRule;
						$auth->add($rule);
					}
			public function actionOwnbreakdown()
							{	
								$auth = Yii::$app->authManager;	
								$rule = new \app\rbac\OwnBreakdownRule;
								$auth->add($rule);
							}
					
					
}?>