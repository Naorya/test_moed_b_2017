<?php							
namespace app\rbac;
use yii\rbac\Rule;
use Yii; 

	class OwnBreakdownRule extends Rule
			{
	public $name = 'ownBreakdownRule';	
				public function execute($user, $item, $params)
			{
				if (!Yii::$app->user->isGuest) {
						return isset($params['Breakdown']) ? $params['Breakdown']->teamleader == $user : false;
					}
			return false;
				}
} ?>