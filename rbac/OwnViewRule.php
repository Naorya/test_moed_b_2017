<?php							
namespace app\rbac;
use yii\rbac\Rule;
use Yii; 

		class OwnViewRule extends Rule
			{
				public $name = 'ownViewRule';	
				public function execute($user, $item, $params)
				{
				if(isset($user)){
				$currentUserRole = \Yii::$app->authManager->getRolesByUser($user);
				
				if($currentUserRole == 'manager')
					return false;
					}
								return false;
				}
	} ?>