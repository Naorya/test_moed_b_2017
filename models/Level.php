<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "level".
 *
 * @property integer $id
 * @property string $level_name
 */
class Level extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'level';
    }
	public static function getLevels()
    {
        $allLevels = self::find()->all();
        $allLevelsArray = ArrayHelper::
                    map($allLevels, 'id','level_name');
        return $allLevelsArray; 
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['level_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'level_name' => 'Level Name',
        ];
    }
}
