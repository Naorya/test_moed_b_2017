<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170803_135933_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
			'username' => $this->string(),
            'password' => $this->string(),
            'authKey' => $this->string(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
