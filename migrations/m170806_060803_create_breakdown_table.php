<?php

use yii\db\Migration;

/**
 * Handles the creation of table `breakdown`.
 */
class m170806_060803_create_breakdown_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('breakdown', [
            'id' => $this-> primaryKey(),
            'title'=> $this-> string(),
            'level'=> $this-> Integer(),
            'status' => $this->Integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('breakdown');
    }
}
